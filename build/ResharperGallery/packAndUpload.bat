SET NUGET=..\..\tools\nuget\NuGet.exe

%NUGET% Update -self
%NUGET% Pack Greenkeeper.nuspec -Symbols
%NUGET% Push Greenkeeper.1.1.0.symbols.nupkg -ApiKey XXX -Source https://resharper-plugins.jetbrains.com
