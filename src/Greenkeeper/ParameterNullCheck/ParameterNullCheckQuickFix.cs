﻿using System.Collections.Generic;
using JetBrains.ReSharper.Feature.Services.Bulbs;
using JetBrains.ReSharper.Intentions.Extensibility;
using JetBrains.ReSharper.Intentions.Extensibility.Menu;
using JetBrains.Util;
using System;

namespace Greenkeeper.ParameterNullCheck
{
    [QuickFix]
    public class ParameterNullCheckQuickFix : IQuickFix
    {
        private readonly ParameterNullCheckHighlighting _Highlighting;
     
        public ParameterNullCheckQuickFix(ParameterNullCheckHighlighting highlighting)
        {
            if (highlighting == null) throw new ArgumentNullException("highlighting");
            _Highlighting = highlighting;
        }
        
        public IEnumerable<IntentionAction> CreateBulbItems()
        {
            return GetParameterNullCheckBulbAction().ToQuickFixAction();
        }

        public bool IsAvailable(IUserDataHolder cache)
        {
          return _Highlighting.IsValid();
        }
        
        private ParameterNullCheckBulbAction GetParameterNullCheckBulbAction()
        {
            return new ParameterNullCheckBulbAction(_Highlighting.GetFunctionWithParameter(), _Highlighting.GetSettings());
        }
    }
}
