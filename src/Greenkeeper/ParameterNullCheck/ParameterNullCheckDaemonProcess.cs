﻿using JetBrains.Application.Settings;
using JetBrains.DocumentModel;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Daemon;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp;
using JetBrains.ReSharper.Psi.Files;
using JetBrains.ReSharper.Psi.Tree;
using NLog;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Greenkeeper.ParameterNullCheck
{
    public class ParameterNullCheckDaemonProcess : IDaemonStageProcess
    {
        private readonly IDaemonProcess _DaemonProcess;
        private readonly IContextBoundSettingsStore _SettingsStore;
        private readonly Logger _Logger;
        private ParameterNullCheckSettings _Settings;

        public ParameterNullCheckDaemonProcess(IDaemonProcess daemonProcess, IContextBoundSettingsStore settingsStore)
        {
            if (daemonProcess == null) throw new ArgumentNullException("daemonProcess");
            if (settingsStore == null) throw new ArgumentNullException("settingsStore");
            _DaemonProcess = daemonProcess;
            _SettingsStore = settingsStore;
            _Logger = LogManager.GetCurrentClassLogger();
        }
        
        public IDaemonProcess DaemonProcess
        {
          get { return _DaemonProcess; }
        }

        private string GetDaemonLogDescription()
        {
            return "ParameterNullCheckDaemonProcessExecute on " + _DaemonProcess.SourceFile.Name;
        }

        public void Execute(Action<DaemonStageResult> commiter)
        {
            ExceptionLogger.Execute(_Logger, GetDaemonLogDescription(), () => CommitDaemonStageResultAndLogTrackedTime(commiter));
        }

        private void CommitDaemonStageResultAndLogTrackedTime(Action<DaemonStageResult> commiter)
        {
            TimeTrackerLogger.Execute(_Logger, LogLevel.Info, GetDaemonLogDescription(), () => CommitDaemonStageResult(commiter));
        }

        private void CommitDaemonStageResult(Action<DaemonStageResult> commiter)
        {
            if (_DaemonProcess.SourceFile.LanguageType is HtmlProjectFileType)
            {
                return;
            }

            IFile file = _DaemonProcess.SourceFile.GetTheOnlyPsiFile(CSharpLanguage.Instance);
            if (file == null)
                return;

            commiter(GetDaemonStageResult(file));
        }

        private DaemonStageResult GetDaemonStageResult(IFile file)
        {
            return new DaemonStageResult(GetHighlightingInfos(file));
        }

        private void LoadParameterNullCheckSettings()
        {
            var loader = new ParameterNullCheckSettingsLoader(_SettingsStore);
            _Settings = loader.Load();
        }

        private ICollection<HighlightingInfo> GetHighlightingInfos(IFile file)
        {
            LoadParameterNullCheckSettings();
            var functionNullCheckDetector = new ParameterNullCheckInFileRequiredDetector(file, _Settings);
            var functionsNeedNullCheck = functionNullCheckDetector.GetFunctionsNeedNullCheck();
            
            var highlightings = new Collection<HighlightingInfo>();
            foreach (var functionWithParameter in functionsNeedNullCheck)
            {
               highlightings.Add(GetHighlightingInfos(functionWithParameter));
            }
            return highlightings;
        }

        private HighlightingInfo GetHighlightingInfos(ICSharpFunctionWithParameterProvider functionWithParamter)
        {
            var parameterDeclaration = functionWithParamter.GetParametersOwnerDeclaration();
            return new HighlightingInfo(parameterDeclaration.Params.GetDocumentRange(),
                                        new ParameterNullCheckHighlighting(functionWithParamter, _Settings));
        }
    }
}
