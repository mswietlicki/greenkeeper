﻿using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using System;

namespace Greenkeeper.ParameterNullCheck
{
    public class ParameterOutputCriterion : INullCheckRequiredCriterion
    {
        public bool RequireNullCheck(ICSharpFunctionDeclaration functionDeclaration, ICSharpParameterDeclaration parameterDeclaration)
        {
            if (functionDeclaration == null) throw new ArgumentNullException("functionDeclaration");
            if (parameterDeclaration == null) throw new ArgumentNullException("parameterDeclaration");
            if (parameterDeclaration.DeclaredElement == null) throw new NullReferenceException("parameterDeclaration.DeclaredElement");
            return parameterDeclaration.DeclaredElement.Kind != ParameterKind.OUTPUT;
        }
    }
}
