﻿using JetBrains.Annotations;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Greenkeeper.ParameterNullCheck
{
    public class StatementMultiNullCheckExposer : IStatementNullCheckExposer
    {
        private readonly ICollection<IStatementNullCheckExposer> _NullCheckExposers;

        public StatementMultiNullCheckExposer(ICollection<IStatementNullCheckExposer> nullCheckExposers)
        {
            if (nullCheckExposers == null) throw new ArgumentNullException("nullCheckExposers");
            _NullCheckExposers = nullCheckExposers;
        }

        public ICollection<ICSharpStatement> GetNullCheckForParameterConsiderOnlyCondition(
            [NotNull] ICSharpFunctionDeclaration functionDeclaration,
            [NotNull] ICSharpParameterDeclaration parameterDeclaration, 
            [NotNull] IExposerContext exposerContext)
        {
            if (functionDeclaration == null) throw new ArgumentNullException("functionDeclaration");
            if (parameterDeclaration == null) throw new ArgumentNullException("parameterDeclaration");
            if (exposerContext == null) throw new ArgumentNullException("exposerContext");
            return
                _NullCheckExposers.SelectMany(
                    e =>
                    e.GetNullCheckForParameterConsiderOnlyCondition(functionDeclaration, parameterDeclaration,
                                                                    exposerContext)).ToList();
        }

        public ICollection<ICSharpStatement> GetNullChecksForParameter(
            [NotNull] ICSharpFunctionDeclaration functionDeclaration,
            [NotNull] ICSharpParameterDeclaration parameterDeclaration, 
            [NotNull] IExposerContext exposerContext)
        {
            if (functionDeclaration == null) throw new ArgumentNullException("functionDeclaration");
            if (parameterDeclaration == null) throw new ArgumentNullException("parameterDeclaration");
            if (exposerContext == null) throw new ArgumentNullException("exposerContext");
            return
                _NullCheckExposers.SelectMany(
                    e => e.GetNullChecksForParameter(functionDeclaration, parameterDeclaration, exposerContext))
                                  .ToList();
        }
    }
}
