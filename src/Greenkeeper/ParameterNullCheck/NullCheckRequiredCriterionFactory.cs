﻿using System;
using System.Collections.ObjectModel;

namespace Greenkeeper.ParameterNullCheck
{
    public static class NullCheckRequiredCriterionFactory
    {
        public static INullCheckRequiredCriterion CreateCriterion(ParameterNullCheckSettings settings)
        {
            if (settings == null) throw new ArgumentNullException("settings");

            var criteria = new Collection<INullCheckRequiredCriterion>
                {
                    new ParameterValueTypeCriterion(),
                    new ParameterOutputCriterion(),
                    new ParameterCanBeNullAttributeCriterion(),
                    new OptionalNullParameterCriterion(),
                    new FunctionAttributeCriterion(settings),
                    new InterfaceContainsFunctionCriterion(),
                    new IsNoAbstractFunctionCriterion()
                };
            return new AndCriterion(criteria);
        }
    }
}
