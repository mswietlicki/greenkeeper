﻿using System;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.ParameterNullCheck
{
    public class IsNoAbstractFunctionCriterion : INullCheckRequiredCriterion
    {
        public bool RequireNullCheck(ICSharpFunctionDeclaration functionDeclaration, ICSharpParameterDeclaration parameterDeclaration)
        {
            if (functionDeclaration == null) throw new ArgumentNullException("functionDeclaration");
            if (parameterDeclaration == null) throw new ArgumentNullException("parameterDeclaration");
            return !functionDeclaration.IsAbstract;
        }
    }
}