﻿using JetBrains.ProjectModel;
using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Greenkeeper.ParameterNullCheck
{
    public class ParameterNullCheckInFunctionInserter
    {
        private readonly ICSharpFunctionWithParameterProvider _Declaration;
        private readonly ISolution _Solution;
        private const string _ArgumentNullExceptionName = "ArgumentNullException";
        private const string _NullCheckFormat = "if ({0} == null) throw new " + _ArgumentNullExceptionName + "(\"{0}\");";
        private readonly ICSharpFunctionDeclaration _FunctionDeclaration;
        private readonly ICSharpParametersOwnerDeclaration _ParametersDeclaration;
        private const string _NotNullAttributeName = "NotNull";
        private readonly ParameterNullCheckSettings _ParameterNullCheckSettings;

        public ParameterNullCheckInFunctionInserter(ICSharpFunctionWithParameterProvider declaration, ISolution solution, ParameterNullCheckSettings parameterNullCheckSettings)
        {
            if (declaration == null) throw new ArgumentNullException("declaration");
            if (solution == null) throw new ArgumentNullException("solution");
            if (parameterNullCheckSettings == null) throw new ArgumentNullException("parameterNullCheckSettings");

            _Declaration = declaration;
            _Solution = solution;
            _ParameterNullCheckSettings = parameterNullCheckSettings;
            _FunctionDeclaration = declaration.GetFunctionDeclaration();
            _ParametersDeclaration = declaration.GetParametersOwnerDeclaration();

            if (_FunctionDeclaration == null) throw new NullReferenceException("_FunctionDeclaration");
            if (_ParametersDeclaration == null) throw new NullReferenceException("_ParametersDeclaration");
        }

        public void InsertParameterNullCheck()
        {
            using (var functionWriter = new FunctionWriter(_FunctionDeclaration, _Solution))
            {
                RemoveExistingParameterNullCheck(functionWriter);
                CreateNullCheckForParameters(functionWriter);
                functionWriter.CommitTransaction();
            }
        }

        private void RemoveExistingParameterNullCheck(FunctionWriter functionWriter)
        {
            foreach (var param in _ParametersDeclaration.ParameterDeclarations)
            {
                var statementExposer = StatementNullCheckExposerFactory.CreateExposer();
                var statements = statementExposer.GetNullCheckForParameterConsiderOnlyCondition(_FunctionDeclaration, param, new ExposerContext());

                foreach (var cSharpStatement in statements)
                {
                    if (StatementIsChildOfFunctionDeclaration(cSharpStatement))
                    {
                        functionWriter.RemoveStatement(cSharpStatement);
                    }
                }
            }
        }

        private bool StatementIsChildOfFunctionDeclaration(ICSharpStatement cSharpStatement)
        {
            return cSharpStatement.PathToRoot().Any(n => n.Equals(_FunctionDeclaration));
        }

        private void CreateNullCheckForParameters(FunctionWriter functionWriter)
        {
            var index = 0;
            foreach (var parameterToNullCheck in GetParametersToNullCheck())
            {
                var parameter = parameterToNullCheck.ParameterDeclaration;
                if (parameterToNullCheck.ParameterNeedNullCheck && _ParameterNullCheckSettings.AddNotNullAttributeToParameter)
                {
                    InsertNotNullAttributeIfNotExists(functionWriter, parameter);
                }

                if (parameterToNullCheck.ParameterHasNotNeededNullCheck)
                {
                    InsertNullCheckForParameter(functionWriter, parameter, index++);
                }
            }
        }

        private static void InsertNullCheckForParameter(FunctionWriter functionWriter, IParameterDeclaration parameter, int index)
        {
            var paramName = parameter.DeclaredName;
            functionWriter.InsertAtIndex(GetNullCheckForParameter(paramName), index);
        }

        private static void InsertNotNullAttributeIfNotExists(FunctionWriter functionWriter, IParameterDeclaration parameter)
        {
            var regularParam = parameter as IRegularParameterDeclaration;
            if (regularParam != null && !ParameterContainsNotNullAttribute(regularParam))
            {
                var attribute = CreateAttribute(_NotNullAttributeName, functionWriter.ElementFactory);
                regularParam.AddAttributeAfter(attribute, null);
            }
        }

        private static bool ParameterContainsNotNullAttribute(IRegularParameterDeclaration regularParameter)
        {
            var extractor = new AttributesExtractor(regularParameter);
            var attributes = extractor.GetAttributes();
            return attributes.Any(a => _NotNullAttributeName.Equals(a));
        }

        private IEnumerable<ParameterToNullCheck> GetParametersToNullCheck()
        {
            var detector = new ParameterNullCheckInFunctionRequiredDetector(_Declaration,_ParameterNullCheckSettings);
            return detector.YieldParametersToNullCheck();
        }
        
        private static string GetNullCheckForParameter(string paramName)
        {
            return String.Format(_NullCheckFormat, paramName);
        }

        private static IAttribute CreateAttribute(string attributeName, CSharpElementFactory elementFactory)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("[");
            stringBuilder.Append(attributeName);
            stringBuilder.Append("]class c{}");
            var attribute = elementFactory.CreateTypeMemberDeclaration(stringBuilder.ToString(), new object[]{}).Attributes[0];
            if (attribute == null)
                throw new ElementFactoryException("Cannot create Attribute");
            return attribute;
        }
    }
}
