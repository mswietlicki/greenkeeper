﻿using System;
using System.Linq;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.ParameterNullCheck
{
    public class InterfaceContainsFunctionCriterion : INullCheckRequiredCriterion
    {
        public bool RequireNullCheck(ICSharpFunctionDeclaration functionDeclaration, ICSharpParameterDeclaration parameterDeclaration)
        {
            if (functionDeclaration == null) throw new ArgumentNullException("functionDeclaration");
            if (parameterDeclaration == null) throw new ArgumentNullException("parameterDeclaration");

            var path =  functionDeclaration.PathToRoot();
            return !path.OfType<IInterfaceDeclaration>().Any();
        }
    }
}
