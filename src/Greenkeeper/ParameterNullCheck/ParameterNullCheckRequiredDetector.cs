﻿using JetBrains.ReSharper.Psi.CSharp.Tree;
using System;
using System.Linq;

namespace Greenkeeper.ParameterNullCheck
{
    public class ParameterNullCheckRequiredDetector
    {
        private readonly ICSharpFunctionDeclaration _FunctionDeclaration;
        private readonly ICSharpParameterDeclaration _ParameterDeclaration;
        private readonly ParameterNullCheckSettings _Settings;

        public ParameterNullCheckRequiredDetector(ICSharpFunctionDeclaration functionDeclaration, ICSharpParameterDeclaration parameterDeclaration, ParameterNullCheckSettings settings)
        {
            if (functionDeclaration == null) throw new ArgumentNullException("functionDeclaration");
            if (parameterDeclaration == null) throw new ArgumentNullException("parameterDeclaration");
            if (settings == null) throw new ArgumentNullException("settings");
            _FunctionDeclaration = functionDeclaration;
            _ParameterDeclaration = parameterDeclaration;
            _Settings = settings;
        }
        
        public ParameterToNullCheck GetParameterToNullCheck()
        {
            if (ParameterNeedNullCheck())
            {
                var cSharpStatement = LookForParametersNullCheckStatement();
                return new ParameterToNullCheck(true, _ParameterDeclaration, cSharpStatement);
            }
            return new ParameterToNullCheck(false, _ParameterDeclaration);
        }

        private bool ParameterNeedNullCheck()
        {
            var criterion = NullCheckRequiredCriterionFactory.CreateCriterion(_Settings);
            return criterion.RequireNullCheck(_FunctionDeclaration, _ParameterDeclaration);
        }
        
        private ICSharpStatement LookForParametersNullCheckStatement()
        {
            var statementExposer = StatementNullCheckExposerFactory.CreateExposer();
            return statementExposer.GetNullChecksForParameter(_FunctionDeclaration,_ParameterDeclaration, new ExposerContext()).FirstOrDefault();
        }
    }
}