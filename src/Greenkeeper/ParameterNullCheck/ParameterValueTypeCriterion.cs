﻿using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using System;

namespace Greenkeeper.ParameterNullCheck
{
    public class ParameterValueTypeCriterion : INullCheckRequiredCriterion
    {
        public bool RequireNullCheck(ICSharpFunctionDeclaration functionDeclaration, ICSharpParameterDeclaration parameterDeclaration)
        {
            if (functionDeclaration == null) throw new ArgumentNullException("functionDeclaration");
            if (parameterDeclaration == null) throw new ArgumentNullException("parameterDeclaration");
            return parameterDeclaration.Type.Classify.HasValue && parameterDeclaration.Type.Classify.Value != TypeClassification.VALUE_TYPE;
        }
    }
}
