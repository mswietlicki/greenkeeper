﻿using System;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.ParameterNullCheck
{
    public class IfStatementThrowArgumentNullExceptionData
    {
        public ICSharpExpression Condition { get; private set; }
        public bool IsValid { get; private set; }
        public string ThrowedParameterName { get; private set; }

        public IfStatementThrowArgumentNullExceptionData(bool valid, ICSharpExpression condition,
                                                         string throwedParameterName)
        {
            IsValid = valid;
            Condition = condition;
            ThrowedParameterName = throwedParameterName;
        }
    }
}
