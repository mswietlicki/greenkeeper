﻿using Greenkeeper.ParameterNullCheck;
using JetBrains.ReSharper.Daemon;
using JetBrains.ReSharper.Psi.CSharp;
using System;

[assembly:  RegisterConfigurableSeverity(
            ParameterNullCheckHighlighting.SeverityId,
            null,
            HighlightingGroupIds.CodeSmell,
            ParameterNullCheckHighlighting.Title,
            ParameterNullCheckHighlighting.Description,
            Severity.WARNING,
            false)]

namespace Greenkeeper.ParameterNullCheck
{
    [ConfigurableSeverityHighlighting(SeverityId, CSharpLanguage.Name, OverlapResolve = OverlapResolveKind.WARNING)]
    public class ParameterNullCheckHighlighting : IHighlighting
    {
        public const string SeverityId = "NoParameterNullCheckForPublicFunctions";
        public const string Title = "No null check for parameters in public ctor or method";
        public const string Description = "There are not all parameters checked against null for public ctor or method.";

        private readonly ICSharpFunctionWithParameterProvider _FunctionWithParameter;
        private readonly ParameterNullCheckSettings _Settings;

        public ParameterNullCheckHighlighting(ICSharpFunctionWithParameterProvider functionWithParameter,ParameterNullCheckSettings settings)
        {
            if (functionWithParameter == null) throw new ArgumentNullException("functionWithParameter");
            if (settings == null) throw new ArgumentNullException("settings");
            _FunctionWithParameter = functionWithParameter;
            _Settings = settings;
        }

        public string ToolTip
        {
            get { return Description; }
        }

        public string ErrorStripeToolTip
        {
            get { return ToolTip; }
        }

        public int NavigationOffsetPatch
        {
            get { return 0; }
        }

        public bool IsValid()
        {
            return true;
        }

        public ParameterNullCheckSettings GetSettings()
        {
            return _Settings;
        }

        public ICSharpFunctionWithParameterProvider GetFunctionWithParameter()
        {
            return _FunctionWithParameter;
        }
    }
}
