﻿using JetBrains.Application.Settings;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Greenkeeper.ParameterNullCheck
{
    [SettingsKey(typeof(EnvironmentSettings), "ParameterNullCheckSettings")]
    public class ParameterNullCheckSettings
    {
        private static readonly char[] _Separators = new []{',',';'};

        [SettingsEntry(true, "AddNotNullAttributeToParameter")]
        public bool AddNotNullAttributeToParameter { get; set; }

        [SettingsEntry("Test,TestCase,TestCaseSource", "FunctionAttributesThatIndicatesNoNullCheck")]
        public string FunctionAttributesThatIndicatesNoNullCheck { get; set; }

        public ICollection<string> GetFunctionAttributesThatIndicatesNoNullCheck()
        {
            if (string.IsNullOrEmpty(FunctionAttributesThatIndicatesNoNullCheck))
            {
                return new Collection<string>();
            }

            return FunctionAttributesThatIndicatesNoNullCheck.Split(_Separators, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}