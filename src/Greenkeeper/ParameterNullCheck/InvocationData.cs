﻿using System.Collections.Generic;

namespace Greenkeeper.ParameterNullCheck
{
    public class InvocationData
    {
        public ICollection<string> Arguments { get; private set; }
        public string InvocationText { get; private set; }
        public bool IsValid { get; private set; }

        public InvocationData(bool valid, string invocationText, ICollection<string> argumnents)
        {
            IsValid = valid;
            InvocationText = invocationText;
            Arguments = argumnents;
        }
    }
}