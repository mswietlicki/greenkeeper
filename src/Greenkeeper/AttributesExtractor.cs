﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper
{
    public class AttributesExtractor
    {
        private readonly IAttributesOwnerDeclaration _AttributesOwnerDeclaration;

        public AttributesExtractor(IAttributesOwnerDeclaration attributesOwnerDeclaration)
        {
            if (attributesOwnerDeclaration == null) throw new ArgumentNullException("attributesOwnerDeclaration");
            _AttributesOwnerDeclaration = attributesOwnerDeclaration;
        }

        public ICollection<string> GetAttributes()
        {
            var attributes = _AttributesOwnerDeclaration.Attributes;
            return attributes.Select(a => a.Name.QualifiedName).ToList();
        }
    }
}
 