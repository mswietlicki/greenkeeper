﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Psi.ExtensionsAPI.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper
{
    public class TreeNodeWriter : ElementWriter
    {
        private readonly ITreeNode _RootNode;
        private readonly ITreeNodeProvider _TreeNodeProvider;

        public TreeNodeWriter(ITreeNode rootNode, ITreeNodeProvider treeNodeProvider, ISolution solution)
            : base(rootNode, solution)
        {
            if (rootNode == null) throw new ArgumentNullException("rootNode");
            if (treeNodeProvider == null) throw new ArgumentNullException("treeNodeProvider");

            _RootNode = rootNode;
            _TreeNodeProvider = treeNodeProvider;
        }

        public void RemoveTreeNode(ITreeNode treeNode)
        {
            EnsureWriteWithLock(() => ModificationUtil.DeleteChild(treeNode));
        }

        public void InsertAtIndex(ITreeNode treeNode, int index)
        {
            InsertAtIndexTemplateMethod(treeNode, index);
        }

        protected override bool IsEmpty()
        {
            return GetTreeNodes().Count == 0;
        }

        private IList<ITreeNode> GetTreeNodes()
        {
            return _TreeNodeProvider.GetTreeNodes();
        }

        protected override bool IsEndOfElement(int index)
        {
            return index >= GetTreeNodes().Count;
        }

        protected override void Insert(object toInsert)
        {
            ModificationUtil.AddChild(_RootNode, (ITreeNode) toInsert);
        }

        protected override void InsertAfterLast(object toInsert)
        {
            ModificationUtil.AddChildAfter(GetLastTreeNode(), (ITreeNode)toInsert);
        }
        
        private ITreeNode GetLastTreeNode()
        {
            return GetTreeNodes().Last();
        }

        protected override void InsertBefore(object toInsert, int index)
        {
            ModificationUtil.AddChildBefore(GetTreeNodeAtIndex(index), (ITreeNode)toInsert);
        }

        public void InsertBefore(ITreeNode anchor, ITreeNode toInsert)
        {
            EnsureWriteWithLock(() => ModificationUtil.AddChildBefore(anchor, toInsert));
        }

        public void InsertAfter(ITreeNode anchor, ITreeNode toInsert)
        {
            EnsureWriteWithLock(() =>ModificationUtil.AddChildAfter(anchor, toInsert));
        }

        private ITreeNode GetTreeNodeAtIndex(int index)
        {
            return GetTreeNodes()[index];
        }
    }
}
