﻿using NLog;
using System;

namespace Greenkeeper
{
    public class ExceptionLogger
    {
        private readonly Logger _Logger;
        private readonly string _Message;

        public static void Execute(Logger logger, string message, Action toExecute)
        {
            Execute(logger,message,() => ExecuteWithFakeResult(toExecute));
        }

        private static bool ExecuteWithFakeResult(Action toExecute)
        {
            toExecute();
            return true;
        }

        public static T Execute<T>(Logger logger, string message, Func<T> toExecute)
        {
            var exceptionLogger = new ExceptionLogger(logger, message);
            return exceptionLogger.Execute(toExecute);
        }

        public ExceptionLogger(Logger logger, string message)
        {
            _Message = message;
            _Logger = logger;
        }

        public T Execute<T>(Func<T> toExecute)
        {
            try
            {
                return toExecute();
            }
            catch (Exception ex)
            {
                _Logger.ErrorException(_Message,ex);
                throw;
            }
        }
    }
}
