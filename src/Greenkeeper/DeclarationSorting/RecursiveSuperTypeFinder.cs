﻿using JetBrains.ReSharper.Psi;
using JetBrains.ReSharper.Psi.CSharp.Tree;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Greenkeeper.DeclarationSorting
{
    public class RecursiveSuperTypeFinder
    {
        private const string _SystemObjectName = "System.Object";
        private readonly IClassLikeDeclaration _ClassLikeDeclaration;
        private readonly ICollection<IDeclaredType> _SuperTypes;

        public RecursiveSuperTypeFinder(IClassLikeDeclaration classLikeDeclaration)
        {
            if (classLikeDeclaration == null) throw new ArgumentNullException("classLikeDeclaration");
           
            _ClassLikeDeclaration = classLikeDeclaration;
            _SuperTypes = new Collection<IDeclaredType>();
        }

        public ICollection<IDeclaredType> Find()
        {
            if (_ClassLikeDeclaration.DeclaredElement == null) throw new NullReferenceException("_ClassLikeDeclaration.DeclaredElement");
            AddFoundSuperTypes(_ClassLikeDeclaration.DeclaredElement.GetSuperTypes());
            PushObjectSuperTypesToEnd();
            return _SuperTypes;
        }

        private void AddFoundSuperTypes(IEnumerable<IDeclaredType> superTypes)
        {
            foreach (var superType in superTypes)
            {
                _SuperTypes.Add(superType);
                AddFoundSuperTypes(superType.GetSuperTypes());
            }
        }

        private void PushObjectSuperTypesToEnd()
        {
            foreach (var superType in _SuperTypes.ToList())
            {
                if (IsObjectSuperType(superType))
                {
                    _SuperTypes.Remove(superType);
                    _SuperTypes.Add(superType);
                }
            }
        }

        private static bool IsObjectSuperType(IDeclaredType superType)
        {
            return superType.GetClrName().FullName == _SystemObjectName;
        }
    }
}