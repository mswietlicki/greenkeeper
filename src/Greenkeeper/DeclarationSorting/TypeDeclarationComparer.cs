﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Greenkeeper.DeclarationSorting
{
    public class TypeDeclarationComparer : ReferenceDeclarationComparer<string>
    {
       public TypeDeclarationComparer(IList<ElementTypeName> attributeOrder)
            : this(IndexItem<ElementTypeName>.CreateIndexItems(attributeOrder))
        {
        }

       public TypeDeclarationComparer(IList<IndexItem<ElementTypeName>> attributeOrder)
            : base(GetIndexEnumAsStrings(attributeOrder))
        {
        }

        protected override ICollection<string> GetReferencesFrom(DeclarationOrderElement declarationOrderElement)
        {
            var extractor = DeclarationTypeNameExtractor.Create(declarationOrderElement);
            var typeName = extractor.GetTypeName();

            ElementTypeName type;
            if (Enum.TryParse(typeName, true, out type))
            {
                return new Collection<string>{ type.ToString() };
            }
            return new Collection<string>() ;
        }
    }
}