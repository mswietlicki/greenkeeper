﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using JetBrains.ReSharper.Feature.Services.Bulbs;
using JetBrains.ReSharper.Intentions.Extensibility;
using JetBrains.ReSharper.Intentions.Extensibility.Menu;
using JetBrains.Util;

namespace Greenkeeper.DeclarationSorting
{
    [QuickFix]
    public class DeclarationSorterQuickFix : IQuickFix
    {
        private readonly DeclarationSorterHighlighting _Highlighting;

        public DeclarationSorterQuickFix(DeclarationSorterHighlighting highlighting)
        {
            if (highlighting == null) throw new ArgumentNullException("highlighting");
            _Highlighting = highlighting;
        }
        
        public IEnumerable<IntentionAction> CreateBulbItems()
        {
            return GetDeclarationSorterBulbAction().ToQuickFixAction();
        }

        public bool IsAvailable(IUserDataHolder cache)
        {
          return _Highlighting.IsValid();
        }
        
        private DeclarationSorterBulbAction GetDeclarationSorterBulbAction()
        {
            return new DeclarationSorterBulbAction(_Highlighting.GetClassDeclaration(),_Highlighting.GetDeclarationSorter(),_Highlighting.GetAdditionalExecuter());
        }
    }
}
