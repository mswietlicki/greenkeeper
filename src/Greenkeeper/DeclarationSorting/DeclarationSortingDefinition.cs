﻿using System;

namespace Greenkeeper.DeclarationSorting
{
    public class DeclarationSortingDefinition
    {
        public IDeclarationSorter DeclarationSorter { get; private set; }
        public IAdditionalExecutor AdditionalExecutor { get; private set; }

        public DeclarationSortingDefinition(IDeclarationSorter declarationSorter, IAdditionalExecutor additionalExecutor)
        {
            if (declarationSorter == null) throw new ArgumentNullException("declarationSorter");
            if (additionalExecutor == null) throw new ArgumentNullException("additionalExecutor");
            DeclarationSorter = declarationSorter;
            AdditionalExecutor = additionalExecutor;
        }
    }
}
