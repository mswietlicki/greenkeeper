﻿using System.Collections.Generic;

namespace Greenkeeper.DeclarationSorting
{
    public abstract class DefaultDeclarationComparer : IDeclarationComparer
    {
        public virtual void InitWithDeclarationOrderElement(IList<DeclarationOrderElement> declarations)
        {
        }

        public virtual bool NeedResort()
        {
            return false;
        }

        public bool NeedSortedInitWithDeclarationOrderElements()
        {
            return false;
        }

        public abstract int Compare(DeclarationOrderElement x, DeclarationOrderElement y);
    }
}
