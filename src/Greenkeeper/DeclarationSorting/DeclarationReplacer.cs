﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    class DeclarationReplacer
    {
        private readonly ICollection<IDeclarationReplaceItem> _DeclarationReplaceItems;

        public static DeclarationReplacer CreateDefaultDeclarationReplacer()
        {
            return new DeclarationReplacer(GetDefaultDeclarationReplaceItems());
        }

        public static ICollection<IDeclarationReplaceItem> GetDefaultDeclarationReplaceItems()
        {
            return new Collection<IDeclarationReplaceItem>
                {
                    new FieldDeclarationReplaceItem(),
                    new ConstantDeclarationReplaceItem(),
                    new EventDeclarationReplaceItem()
                };
        }

        public DeclarationReplacer(ICollection<IDeclarationReplaceItem> declarationReplaceItems)
        {
            if (declarationReplaceItems == null) throw new ArgumentNullException("declarationReplaceItems");
            _DeclarationReplaceItems = declarationReplaceItems;
        }

        public ITreeNode GetReplacedDeclaration(IDeclaration declaration)
        {
            foreach (var declarationReplaceItem in _DeclarationReplaceItems)
            {
                if (declarationReplaceItem.MustReplace(declaration))
                {
                    return declarationReplaceItem.GetReplacement(declaration);
                }
            }
            return declaration;
        }
    }
}
