﻿namespace Greenkeeper.DeclarationSorting
{
    public enum ElementTypeName
    {
        Method,
        Constant,
        Property,
        Field,
        Constructor,
        Destructor,
        Event,
        Indexer,
        Delegate,
        Operator
    }
}
