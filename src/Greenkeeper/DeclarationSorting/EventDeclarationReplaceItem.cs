﻿using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    class EventDeclarationReplaceItem : IDeclarationReplaceItem
    {
        public bool MustReplace(IDeclaration declaration)
        {
            return declaration is IEventDeclaration && declaration.Parent is IMultipleEventDeclaration;
        }

        public ITreeNode GetReplacement(IDeclaration declaration)
        {
            return declaration.Parent;
        }
    }
}
