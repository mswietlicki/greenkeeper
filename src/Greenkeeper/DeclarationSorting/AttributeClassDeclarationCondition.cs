﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class AttributeClassDeclarationCondition : IClassDeclarationCondition
    {
        private readonly ICollection<string> _ValidAttributes;

        public AttributeClassDeclarationCondition(ICollection<string> validAttributes)
        {
            if (validAttributes == null) throw new ArgumentNullException("validAttributes");
            _ValidAttributes = validAttributes;
        }

        public bool CheckCondition(IClassDeclaration classDeclaration)
        {
            if (classDeclaration == null) throw new ArgumentNullException("classDeclaration");
            var attributes = GetAttributes(classDeclaration);
            return ContainsAnyValid(attributes);
        }

        private ICollection<string> GetAttributes(IClassDeclaration classDeclaration)
        {
            var extractor = new AttributesExtractor(classDeclaration);
            return extractor.GetAttributes();
        }

        private bool ContainsAnyValid(ICollection<string> attributes)
        {
            foreach (var validAttribute in _ValidAttributes)
            {
                if (attributes.Any(a => String.Equals(a, validAttribute)))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
