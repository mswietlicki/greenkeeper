﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Greenkeeper.DeclarationSorting
{
    public class IndexItem<T>
    {
        public T Item { get; private set; }
        public int Index { get; private set; }

        public IndexItem(T item, int index)
        {
            Item = item;
            Index = index;
        }

        public static IList<IndexItem<T>> CreateIndexItems(ICollection<T> items)
        {
            if (items == null) throw new ArgumentNullException("items");

            var indexItems = new Collection<IndexItem<T>>();
            int i = 0;
            foreach (var item in items)
            {
                indexItems.Add(new IndexItem<T>(item, i++));
            }
            return indexItems;
        }
    }
}
