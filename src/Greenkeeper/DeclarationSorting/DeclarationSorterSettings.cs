﻿using Greenkeeper.DeclarationSorting.Serialization;
using JetBrains.Application.Settings;

namespace Greenkeeper.DeclarationSorting
{
  [SettingsKey(typeof(EnvironmentSettings), "DeclarationSorterSettings")]
  public class DeclarationSorterSettings
  {
      [SettingsEntry("", "DeclarationSorterDefinition")]
      public string SerializedDeclarationSorterDefinition { get; set; }

      public string GetDefaultSerializedDeclarationSorterDefinition()
      {
          return DefaultDeclarationSortingDefinitionResource.DefaultDeclarationSortingDefinition;
      }

      [SettingsEntry(false, "DeclarationSorterDefinition")]
      public bool UseDeclarationSorterDefinition { get; set; }

  }
}
