﻿using JetBrains.ReSharper.Psi.CSharp.Tree;
using JetBrains.ReSharper.Psi.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public class IndexerTypeNameExtractorItem : IDeclarationTypeNameExtractorItem
    {
        public bool IsType(IDeclaration declaration)
        {
            return declaration is IIndexerDeclaration;
        }

        public string GetTypeName(IDeclaration declaration)
        {
            return ElementTypeName.Indexer.ToString();
        }
    }
}
