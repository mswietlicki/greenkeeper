﻿using JetBrains.ReSharper.Psi;

namespace Greenkeeper.DeclarationSorting
{
    public class ParameterCountDeclarationComparer : DefaultDeclarationComparer
    {
        public override int Compare(DeclarationOrderElement x, DeclarationOrderElement y)
        {
            var xParamCount = GetParameterCount(x);
            var yParamCount = GetParameterCount(y);
            return xParamCount.CompareTo(yParamCount);
        }
        
        private int GetParameterCount(DeclarationOrderElement declarationOrderElement)
        {
            var paramOwner = GetParameterOwner(declarationOrderElement);
            if (paramOwner == null)
            {
                return 0;
            }
            return paramOwner.Parameters.Count;
        }

        private IParametersOwner GetParameterOwner(DeclarationOrderElement declarationOrderElement)
        {
            var declaration = declarationOrderElement.Declaration;
            var parameterOwner = declaration.DeclaredElement as IParametersOwner;
            if (parameterOwner == null)
            {
                return null;
            }
            return parameterOwner;
        }
    }
}
