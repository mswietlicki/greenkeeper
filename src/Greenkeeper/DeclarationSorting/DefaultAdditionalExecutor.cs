﻿using JetBrains.ReSharper.Psi.CSharp.Tree;

namespace Greenkeeper.DeclarationSorting
{
    public abstract class DefaultAdditionalExecutor : IAdditionalExecutor
    {
        protected TreeNodeWriter TreeNodeWriter;

        public virtual void InitWithTreeNodeWriter(TreeNodeWriter treeNodeWriter)
        {
            TreeNodeWriter = treeNodeWriter;
        }

        public abstract void BeforeSort(IClassDeclaration classDeclaration);
        public abstract void AfterSort(IClassDeclaration classDeclaration);
    }
}
