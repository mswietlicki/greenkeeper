﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using JetBrains.ReSharper.Psi;

namespace Greenkeeper.DeclarationSorting
{
    public class ModifiersDeclarationComparer : ReferenceDeclarationComparer<string>
    {
        public ModifiersDeclarationComparer(IList<ModifierName> attributeOrder)
            : this(IndexItem<ModifierName>.CreateIndexItems(attributeOrder))
        {
        }

        public ModifiersDeclarationComparer(IList<IndexItem<ModifierName>> attributeOrder)
            : base(GetIndexEnumAsStrings(attributeOrder))
        {
        }

        protected override ICollection<string> GetReferencesFrom(DeclarationOrderElement declarationOrderElement)
        {
            var declaration = declarationOrderElement.Declaration;
            var modifiersOwner = declaration as IModifiersOwner;
            if (modifiersOwner == null)
            {
                return new Collection<string>();
            }
            var converter = new ModifiersOwnerToModifierNameConverter(modifiersOwner);
            return GetEnumAsStrings(converter.GetModifierNames());            
        }
    }
}
