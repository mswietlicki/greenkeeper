﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("ParameterCountDeclarationComparer")]
    public class ParameterCountDeclarationComparerProxy : DeclarationComparerProxyBase
    {
        public override IDeclarationComparer CreateDeclarationComparer()
        {
            return new ParameterCountDeclarationComparer();
        }
    }
}
