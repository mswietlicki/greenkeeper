﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("AccessRightsDeclarationComparer")]
    public class AccessRightsDeclarationComparerProxy : DeclarationComparerProxyBase
    {
        [XmlAttribute("AccessRightsOrder")]
        public string AccessRightsOrder { get; set; }
        
        public override IDeclarationComparer CreateDeclarationComparer()
        {
            var splitter = new AccessRightsSplitter();
            var accessRightsOrder = splitter.Split(AccessRightsOrder);
            return new AccessRightsDeclarationComparer(accessRightsOrder);
        }
    }
}
