﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("ConditionalDeclarationComparer")]
    public class ConditionalDeclarationComparerProxy : DeclarationComparerProxyBase
    {
        [XmlElement("DeclarationComparer")]
        public XmlSerializable<DeclarationComparerProxyBase> DeclarationComparerProxy { get; set; }

        [XmlElement("DeclarationCondition")]
        public XmlSerializable<DeclarationConditionProxyBase> DeclarationConditionProxy { get; set; }

        public override IDeclarationComparer CreateDeclarationComparer()
        {
            var declarationComparer = DeclarationComparerProxy.Serialized.CreateDeclarationComparer();
            var declarationCondition = DeclarationConditionProxy.Serialized.CreateDeclarationCondition();
            return new ConditionalDeclarationComparer(declarationComparer,declarationCondition);
        }
    }
}
