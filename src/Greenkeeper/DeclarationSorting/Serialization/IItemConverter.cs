﻿namespace Greenkeeper.DeclarationSorting.Serialization
{
    public interface IItemConverter<T>
    {
        T Convert(string value);
    }
}
