﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("MultiDeclarationComparer")]
    public class MultiDeclarationComparerProxy : DeclarationComparerProxyBase
    {
        [XmlElement("DeclarationComparer")]
        public List<XmlSerializable<DeclarationComparerProxyBase>> DeclarationComparerProxies { get; set; }
       

        public MultiDeclarationComparerProxy()
        {
            DeclarationComparerProxies = new List<XmlSerializable<DeclarationComparerProxyBase>>();
        }

        public override IDeclarationComparer CreateDeclarationComparer()
        {
            var comparers = DeclarationComparerProxies.Select(p => p.Serialized.CreateDeclarationComparer()).ToList();
            return new MultiDeclarationComparer(comparers);
        }
    }
}
