﻿namespace Greenkeeper.DeclarationSorting.Serialization
{
    public abstract class DeclarationSorterProxyBase
    {
        public abstract IDeclarationSorter CreateDeclarationSorter();
    }
}
