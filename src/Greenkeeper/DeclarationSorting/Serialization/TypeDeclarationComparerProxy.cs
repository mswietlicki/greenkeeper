﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("TypeDeclarationComparer")]
    public class TypeDeclarationComparerProxy : DeclarationComparerProxyBase
    {
        [XmlAttribute("TypeOrder")]
        public string TypeOrder { get; set; }

        public override IDeclarationComparer CreateDeclarationComparer()
        {
            var splitter = new TypeNameSplitter();
            var typeOrder = splitter.Split(TypeOrder);
            return new TypeDeclarationComparer(typeOrder);
        }
    }
}
