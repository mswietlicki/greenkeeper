﻿using System.Xml.Serialization;

namespace Greenkeeper.DeclarationSorting.Serialization
{
    [XmlType("NoClassDeclarationCondition")]
    public class NoClassDeclarationConditionProxy : ClassDeclarationConditionProxyBase
    {
        public override IClassDeclarationCondition CreateClassDeclarationCondition()
        {
            return new NoClassDeclarationCondition();
        }
    }
}
