﻿namespace Greenkeeper.DeclarationSorting.Serialization
{
    public abstract class AdditionalExecutorProxyBase
    {
        public abstract IAdditionalExecutor CreateAdditionalExecutor();
    }
}