﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Greenkeeper.DeclarationSorting
{
    class DeclarationOrderElementSorter
    {
        private readonly IDeclarationComparer _DeclarationComparer;
        private IList<DeclarationOrderElement> _DeclarationOrderElements;
        private int _CurrentIndex;
        private int _SortedIndex;
        private IList<DeclarationOrderElement> _SortedDeclarationElements;
        private DeclarationOrderElement _PreviousDeclarationOrderElement;
        private DeclarationOrderElement _CurrentDeclarationOrderElement;
        
        public DeclarationOrderElementSorter(IDeclarationComparer declarationComparer, ICollection<DeclarationOrderElement> declarationOrderElements)
        {
            if (declarationComparer == null) throw new ArgumentNullException("declarationComparer");
            if (declarationOrderElements == null) throw new ArgumentNullException("declarationOrderElements");

            _DeclarationComparer = declarationComparer;
            _DeclarationOrderElements = declarationOrderElements.ToList();
        }

        public ICollection<DeclarationOrderElement> Sort()
        {
            if (_DeclarationOrderElements.Count < 2)
            {
                return _DeclarationOrderElements;
            }

            ExecuteSort();
            return _SortedDeclarationElements;
        }
        
        private void ExecuteSort()
        {
            do
            {
                InitSortElements();
                SortElements();
                _DeclarationOrderElements = CloneDeclarationOrderElements();

            } while (_DeclarationComparer.NeedResort());
        }

        private void InitSortElements()
        {
            _DeclarationComparer.InitWithDeclarationOrderElement(_DeclarationOrderElements);
            _SortedDeclarationElements = new Collection<DeclarationOrderElement>();
            _CurrentIndex = 0;
            _SortedIndex = 0;
        }

        private void SortElements()
        {
            foreach (var declarationOrderElement in GetOrderedElements())
            {
                _CurrentDeclarationOrderElement = declarationOrderElement;
                AddDeclarationAndSetIndicators();
                _PreviousDeclarationOrderElement = declarationOrderElement;
            }
        }

        private IEnumerable<DeclarationOrderElement> GetOrderedElements()
        {
            return _DeclarationOrderElements
                .OrderBy(d => d, _DeclarationComparer)
                .ThenBy(d => d.NativeIndex);
        }

        private void AddDeclarationAndSetIndicators()
        {
            SetSortedIndex();
            AddToSortedDeclaration();
            _CurrentIndex++;
        }
        
        private int GetCompareResultForPreviousWithCurrent()
        {
            if (_CurrentIndex > 0)
            {
                return _DeclarationComparer.Compare(_PreviousDeclarationOrderElement, _CurrentDeclarationOrderElement);
            }
            return 0;
        }

        private void SetSortedIndex()
        {
            if (GetCompareResultForPreviousWithCurrent() == -1)
            {
                _SortedIndex++;
            }
            _CurrentDeclarationOrderElement.SetSortedIndex(_SortedIndex);
        }

        private void AddToSortedDeclaration()
        {
            _SortedDeclarationElements.Add(_CurrentDeclarationOrderElement);
        }

        private IList<DeclarationOrderElement> CloneDeclarationOrderElements()
        {
            var newDeclarationElements = new Collection<DeclarationOrderElement>();
            foreach (var element in _SortedDeclarationElements)
            {
                newDeclarationElements.Add(new DeclarationOrderElement(element.Declaration, element.NativeIndex, element.SortedIndex));
            }
            return newDeclarationElements;
        }
    }
}
