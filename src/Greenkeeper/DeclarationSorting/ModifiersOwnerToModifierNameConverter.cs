﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using JetBrains.ReSharper.Psi;

namespace Greenkeeper.DeclarationSorting
{
    class ModifiersOwnerToModifierNameConverter
    {
        private readonly IModifiersOwner _ModifiersOwner;
        private ICollection<ModifierName> _ModifierNames;

        public ModifiersOwnerToModifierNameConverter(IModifiersOwner modifiersOwner)
        {
            if (modifiersOwner == null) throw new ArgumentNullException("modifiersOwner");
            _ModifiersOwner = modifiersOwner;
        }

        public ICollection<ModifierName> GetModifierNames()
        {
            _ModifierNames = new Collection<ModifierName>();
            AddAbstract();
            AddExtern();
            AddOverride();
            AddReadonly();
            AddSealed();
            AddStatic();
            AddUnsafe();
            AddVirtual();
            AddVolatile();
            AddNone();
            return _ModifierNames;
        }

        private void AddAbstract()
        {
            if (_ModifiersOwner.IsAbstract)
            {
                _ModifierNames.Add(ModifierName.Abstract);
            }
        }

        private void AddExtern()
        {
            if (_ModifiersOwner.IsExtern)
            {
                _ModifierNames.Add(ModifierName.Extern);
            }
        }

        private void AddOverride()
        {
            if (_ModifiersOwner.IsOverride)
            {
                _ModifierNames.Add(ModifierName.Override);
            }
        }

        private void AddReadonly()
        {
            if (_ModifiersOwner.IsReadonly)
            {
                _ModifierNames.Add(ModifierName.Readonly);
            }
        }

        private void AddSealed()
        {
            if (_ModifiersOwner.IsSealed)
            {
                _ModifierNames.Add(ModifierName.Sealed);
            }
        }

        private void AddStatic()
        {
            if (_ModifiersOwner.IsStatic)
            {
                _ModifierNames.Add(ModifierName.Static);
            }
        }

        private void AddUnsafe()
        {
            if (_ModifiersOwner.IsUnsafe)
            {
                _ModifierNames.Add(ModifierName.Unsafe);
            }
        }

        private void AddVirtual()
        {
            if (_ModifiersOwner.IsVirtual)
            {
                _ModifierNames.Add(ModifierName.Virtual);
            }
        }

        private void AddVolatile()
        {
            if (_ModifiersOwner.IsVolatile)
            {
                _ModifierNames.Add(ModifierName.Volatile);
            }
        }

        private void AddNone()
        {
            if (_ModifierNames.Count == 0)
            {
                _ModifierNames.Add(ModifierName.None);
            }
        }
    }
}
