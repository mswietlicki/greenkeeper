﻿using Greenkeeper.ParameterNullCheck;
using JetBrains.Application.Settings;
using JetBrains.DataFlow;
using JetBrains.UI.CrossFramework;
using JetBrains.UI.Options;
using System;
using System.Windows;

namespace Greenkeeper.Options
{
    [OptionsPage(_PageId, "ParameterNullCheck", typeof(UnnamedThemedIcons.Greenkeeper), ParentId = RootOptionPage.PageId)]
    public partial class ParameterNullCheckOptionPage :  IOptionsPage
    {
        private const string _PageId = "GreenkeeperParameterNullCheckOption";
        private readonly OptionsSettingsSmartContext _SettingsStore;

        public ParameterNullCheckOptionPage(Lifetime lifetime, OptionsSettingsSmartContext settings)
        {
            if (lifetime == null) throw new ArgumentNullException("lifetime");
            if (settings == null) throw new ArgumentNullException("settings");

            InitializeComponent();

            _SettingsStore = settings;
            Loaded += OnParameterNullCheckOptionPageLoaded;
        }

        private void OnParameterNullCheckOptionPageLoaded(object sender, RoutedEventArgs e)
        {
            LoadSettings();
        }

        public EitherControl Control
        {
            get { return this; }
        }

        public string Id
        {
            get { return _PageId; }
        }
        
        public bool OnOk()
        {
            SaveSettings();
            return true;
        }

        public bool ValidatePage()
        {
            return true;
        }

        private void SaveSettings()
        {
            var settings = GetParameterNullCheckSettings();
            _SettingsStore.SetKey(settings, SettingsOptimization.OptimizeDefault);
        }

        private ParameterNullCheckSettings GetParameterNullCheckSettings()
        {
            return new ParameterNullCheckSettings
            {
                AddNotNullAttributeToParameter = IsNotNullAttributeChecked(),
                FunctionAttributesThatIndicatesNoNullCheck = txtFunctionAttributes.Text
            };
        }

        private bool IsNotNullAttributeChecked()
        {
            return checkBoxInsertAttribute.IsChecked.HasValue && checkBoxInsertAttribute.IsChecked.Value;
        }

        private void LoadSettings()
        {
            var loader = new ParameterNullCheckSettingsLoader(_SettingsStore);
            var settings = loader.Load();
            checkBoxInsertAttribute.IsChecked = settings.AddNotNullAttributeToParameter;
            txtFunctionAttributes.Text = settings.FunctionAttributesThatIndicatesNoNullCheck;
        }
    }
}
