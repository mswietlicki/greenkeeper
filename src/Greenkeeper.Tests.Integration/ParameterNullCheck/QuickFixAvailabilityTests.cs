﻿using Greenkeeper.ParameterNullCheck;
using JetBrains.ReSharper.Daemon;
using JetBrains.ReSharper.Intentions.Test;
using JetBrains.ReSharper.Psi;
using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.ParameterNullCheck
{
    [TestFixture]
    public class QuickFixAvailabilityTests : QuickFixAvailabilityTestBase
    {
        protected override string RelativeTestDataPath
        {
            get { return @"Intentions\QuickFixes\ParameterNullCheckAvailability"; }
        }

        protected override bool HighlightingPredicate(IHighlighting highlighting, IPsiSourceFile psiSourceFile)
        {
            return highlighting is ParameterNullCheckHighlighting;
        }

        [TestCase("AvailableOnFuncWithParameter")]
        [TestCase("NotAvailableOnFuncWithParameterAndNullChecks")]
        public void TestExecution(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
