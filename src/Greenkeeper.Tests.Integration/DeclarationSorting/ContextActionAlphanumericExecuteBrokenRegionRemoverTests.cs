﻿using Greenkeeper.DeclarationSorting;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.TextControl;
using NUnit.Framework;
using System.Collections.ObjectModel;

namespace Greenkeeper.Tests.Integration.DeclarationSorting
{
    [TestFixture]
    public class ContextActionAlphanumericExecuteBrokenRegionRemoverTests : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterAlphanumericExecutionBrokenRegionRemover"; }
        }
        
        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var comparer = new AlphanumericDeclarationComparer();
            var declarationSorter = DeclarationSorterFactory.CreateDeclarationSorter(comparer);
            var executor =
                new MultiAdditionalExecutor(new Collection<IAdditionalExecutor>
                    {
                        new RegionKeeperExecutor(),
                        new BrokenRegionRemoverExecutor()
                    });
            return DeclarationSorterContextAction.Create(dataProvider, declarationSorter, executor);
        }


        [TestCase("ExecutionOnUnsortedRemoveAllRegions")]
        [TestCase("ExecutionOnUnsortedKeepAllRegions")]
        [TestCase("ExecutionOnUnsortedKeepAndRemove")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
