﻿using Greenkeeper.DeclarationSorting;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.ReSharper.Psi;
using JetBrains.TextControl;
using NUnit.Framework;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Greenkeeper.Tests.Integration.DeclarationSorting
{
    [TestFixture]
    public class ContextActionAttributeClassDeclarationConditionExecuteTests : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterAttributeClassDeclarationConditionExecution"; }
        }

        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var alphaNumDeclarationSorter =
                 DeclarationSorterFactory.CreateDeclarationSorter(new AlphanumericDeclarationComparer());
            var conditionalAlphaNumDeclarationSorter =
                new ConditionalDeclarationSorter(new NoClassDeclarationCondition(), alphaNumDeclarationSorter);

            var accessRightsOrder = new List<AccessRights> { AccessRights.PUBLIC, AccessRights.INTERNAL, AccessRights.PROTECTED, AccessRights.PRIVATE };
            var accessRightsComparer = new AccessRightsDeclarationComparer(accessRightsOrder);
            var accessRightsDeclarationSorter = DeclarationSorterFactory.CreateDeclarationSorter(accessRightsComparer);

            var validAttributes = new List<string> { "TestFixture" };
            var attributeCondition = new AttributeClassDeclarationCondition(validAttributes);
            var conditionalAccessRightsDeclarationSorter =
                new ConditionalDeclarationSorter(attributeCondition, accessRightsDeclarationSorter);


            var multiDeclarationSorter = new MultiConditionalDeclarationSorter(
                new Collection<ConditionalDeclarationSorter>
                    {
                        conditionalAccessRightsDeclarationSorter,
                        conditionalAlphaNumDeclarationSorter
                    });

            return DeclarationSorterContextAction.Create(dataProvider, multiDeclarationSorter);
        }

        [TestCase("ExecutionOnUnsortedNotMatchFirstCondition")]
        [TestCase("ExecutionOnUnsortedMatchFirstCondition")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
