﻿using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting.Serialization
{
     
    [TestFixture]
    public class ContextActionSerializedAlphanumericExecuteBrokenRegionRemoverTests : ContextActionSerializedExecuteTestBase
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterAlphanumericExecutionBrokenRegionRemover"; }
        }

        protected override string XmlFileName
        {
            get { return "SorterWithRegionComparerAndAdditionalExecution"; }
        }

        [TestCase("ExecutionOnUnsortedRemoveAllRegions")]
        [TestCase("ExecutionOnUnsortedKeepAllRegions")]
        [TestCase("ExecutionOnUnsortedKeepAndRemove")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
