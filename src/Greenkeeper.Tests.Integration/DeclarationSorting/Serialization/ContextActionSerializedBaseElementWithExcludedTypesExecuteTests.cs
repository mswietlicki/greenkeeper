﻿using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting.Serialization
{
     
    [TestFixture]
    public class ContextActionSerializedBaseElementWithExcludedTypesExecuteTests : ContextActionSerializedExecuteTestBase
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterBaseElementExecutionWithExcludedTypes"; }
        }

        protected override string XmlFileName
        {
            get { return "SorterWithDeclarationBaseElementComparer"; }
        }

        [TestCase("ExecutionOnUnsortedWithOneInterfaceAndDisposable")]
        [TestCase("ExecutionOnUnsortedWithOneInterfaceAndObject")]
        [TestCase("ExecutionOnUnsortedWithOneInterfaceAndObjectAndDisposable")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
