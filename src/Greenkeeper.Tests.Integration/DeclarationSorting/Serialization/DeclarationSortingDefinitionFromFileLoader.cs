﻿using System;
using System.IO;
using System.Reflection;
using Greenkeeper.DeclarationSorting;
using Greenkeeper.DeclarationSorting.Serialization;

namespace Greenkeeper.Tests.Integration.DeclarationSorting.Serialization
{
    public class DeclarationSortingDefinitionFromFileLoader
    {
        private readonly string _ExtraPath;
        private readonly string _FileName;
        private const string _FileEnding = ".xml";
        private const string _RemovePathUntil = @"Greenkeeper.Tests.Integration";
        private const string _RelativeTestHomePath = @"test\data\Intentions\ContextActions";

        public DeclarationSortingDefinitionFromFileLoader(string extraPath, string fileName)
        {
            if (extraPath == null) throw new ArgumentNullException("extraPath");
            if (fileName == null) throw new ArgumentNullException("fileName");
            _ExtraPath = extraPath;
            _FileName = fileName;
        }

        public DeclarationSortingDefinition Load()
        {
            var definitionProxy = Deserialize();
            return definitionProxy.Create();;
        }

        private DeclarationSortingDefinitionProxy Deserialize()
        {
            var fileContent = ReadFile();
            var deserializer = new DeclarationSortingDefinitionSerializer(fileContent);
            return deserializer.Deserialize();
        }

        private string ReadFile()
        {
            var filePath = GetFilePath();
            string fileContent;
            using (var streamReader = new StreamReader(filePath))
            {
                fileContent = streamReader.ReadToEnd();
                streamReader.Close();
            }

            return fileContent;
        }

        private string GetFilePath()
        {
            var codeBasePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
            var homePath = GetAbsoluteTestHomePath(codeBasePath);
            return Path.Combine(homePath,_ExtraPath, _FileName + _FileEnding);
        }

        private string GetAbsoluteTestHomePath(string codeBasePath)
        {
            var cleanedCodeBasePath = RemoveUriSchemeFileIfExists(codeBasePath);
            var basePath = ExtractBasePath(cleanedCodeBasePath);
            return Path.Combine(basePath, _RelativeTestHomePath);
        }

        private string RemoveUriSchemeFileIfExists(string path)
        {
            if (path.StartsWith(Uri.UriSchemeFile, StringComparison.Ordinal))
            {
                return path.Substring(Uri.UriSchemeFile.Length + 2);
            }
            return path;
        }

        private string ExtractBasePath(string codeBasePath)
        {
            var relativePathStart = codeBasePath.IndexOf(_RemovePathUntil);
            if (relativePathStart == -1)
            {
                throw  new NotSupportedException("Can not find folder with tests");
            }
            return codeBasePath.Substring(0, relativePathStart);
        }
    }
}
