﻿using Greenkeeper.DeclarationSorting;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.ReSharper.Psi;
using JetBrains.TextControl;
using NUnit.Framework;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Greenkeeper.Tests.Integration.DeclarationSorting
{
    [TestFixture]
    public class ContextActionMultiExecuteAccessRightsAlphanumericTests : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterMultiExecutionAccessRightsAlphanumeric"; }
        }
        
        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var order = new List<AccessRights> { AccessRights.PUBLIC, AccessRights.INTERNAL, AccessRights.PROTECTED, AccessRights.PRIVATE };
            var accessRightsDeclaration = new AccessRightsDeclarationComparer(order);
            var alphanumericDeclaration = new AlphanumericDeclarationComparer();
            var comparer = new MultiDeclarationComparer(
                new Collection<IDeclarationComparer>
                    {
                        accessRightsDeclaration,
                        alphanumericDeclaration
                    });
            var declarationSorter = DeclarationSorterFactory.CreateDeclarationSorter(comparer);
            return DeclarationSorterContextAction.Create(dataProvider, declarationSorter);
        }

        [TestCase("ExecutionOnUnsortedMethods")]
        [TestCase("ExecutionOnUnsortedMix")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
