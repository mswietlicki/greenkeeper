﻿using Greenkeeper.DeclarationSorting;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.TextControl;
using NUnit.Framework;
using System.Collections.Generic;

namespace Greenkeeper.Tests.Integration.DeclarationSorting
{
    [TestFixture]
    public class ContextActionModifiersExecuteTests : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterModifiersExecution"; }
        }
        
        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var order = new List<ModifierName> { ModifierName.Abstract, ModifierName.Static, ModifierName.Readonly };
            var comparer = new ModifiersDeclarationComparer(order);
            var declarationSorter = DeclarationSorterFactory.CreateDeclarationSorter(comparer);
            return DeclarationSorterContextAction.Create(dataProvider, declarationSorter);
        }

        [TestCase("ExecutionOnUnsorted1")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
