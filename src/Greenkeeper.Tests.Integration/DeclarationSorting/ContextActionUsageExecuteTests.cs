﻿using Greenkeeper.DeclarationSorting;
using Greenkeeper.ParameterNullCheck;
using JetBrains.ProjectModel;
using JetBrains.ReSharper.Feature.Services.CSharp.Bulbs;
using JetBrains.ReSharper.Intentions.CSharp.Test;
using JetBrains.ReSharper.Intentions.Extensibility;
using JetBrains.TextControl;
using NUnit.Framework;

namespace Greenkeeper.Tests.Integration.DeclarationSorting
{
    [TestFixture]
    public class ContextActionUsageExecuteTests : CSharpContextActionExecuteTestBase<DeclarationSorterContextAction>
    {
        //BasePathForFilesUnderTest: test\data\Intentions\ContextActions\ + ExtraPath
        protected override string ExtraPath
        {
            get { return "DeclarationSorterUsageExecution"; }
        }
        
        protected override DeclarationSorterContextAction CreateContextAction(ISolution solution, ITextControl textControl)
        {
            var dataProvider = CSharpContextActionDataProviderFactory.Create(solution, textControl);
            var comparer = new UsageOrderDeclarationComparer();
            var declarationSorter = DeclarationSorterFactory.CreateDeclarationSorter(comparer);
            return DeclarationSorterContextAction.Create(dataProvider, declarationSorter);
        }

        [TestCase("ExecutionOnUnsortedMethods1")]
        [TestCase("ExecutionOnUnsortedMethods2")]
        [TestCase("ExecutionOnUnsortedMix1")]
        [TestCase("ExecutionOnUnsortedMethodsWithMultiClasses")]
        [TestCase("ExecutionOnUnsortedProperties1")]
        [TestCase("ExecutionOnUnsortedProperties2")]
        [TestCase("ExecutionOnFieldWithInitializer")]
        [TestCase("ExecutionOnFieldWithInitializerViaMethod")]
        [TestCase("ExecutionOnIndexer")]
        [TestCase("ExecutionOnConstant")]
        [TestCase("ExecutionOnConstructor")]
        [TestCase("ExecutionOnDestructor")]
        public void Test(string fileName)
        {
            DoOneTest(fileName);
        }
    }
}
