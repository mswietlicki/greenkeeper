﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Greenkeeper.DeclarationSorting;
using JetBrains.ReSharper.Psi.Tree;
using NUnit.Framework;
using Rhino.Mocks;

namespace Greenkeeper.Tests.DeclarationSorting
{
    [TestFixture]
    public class DeclarationOrderElementSorterTests
    {
        [Test]
        public void EmptyList_NoExecution()
        {
            var collection = GetDeclarationOrderElements();
            var comparer = MockDeclarationComparer();
           
            var result = Sort(comparer, collection);
            Assert.AreEqual(collection.Count, result.Count);

            comparer.AssertWasNotCalled(c => c.InitWithDeclarationOrderElement(null), o => o.IgnoreArguments());
            comparer.AssertWasNotCalled(c => c.Compare(null, null), o => o.IgnoreArguments());
        }

        private IList<DeclarationOrderElement> Sort(IDeclarationComparer comparer, ICollection<DeclarationOrderElement> orderElements)
        {
            var sorter = new DeclarationOrderElementSorter(comparer, orderElements);
            return sorter.Sort().ToList();
        }

        private IList<DeclarationOrderElement> GetDeclarationOrderElements(params IDeclaration[] declarations)
        {
            var collection = new Collection<DeclarationOrderElement>();
            int i = 0;
            foreach (var declaration in declarations)
            {
                collection.Add(new DeclarationOrderElement(declaration,i++));
            }
            return collection;
        }

        private IDeclarationComparer MockDeclarationComparer()
        {
            return MockRepository.GenerateMock<IDeclarationComparer>();
        }

        [Test]
        public void ListWithOneItem_NoExecution()
        {
            var declaration = MockDeclaration();
            var collection = GetDeclarationOrderElements(declaration);
            var comparer = MockDeclarationComparer();

            var result = Sort(comparer, collection);
            Assert.IsTrue(ReferenceEquals(declaration, result[0].Declaration));

            comparer.AssertWasNotCalled(c => c.InitWithDeclarationOrderElement(null), o => o.IgnoreArguments());
            comparer.AssertWasNotCalled(c => c.Compare(null, null), o => o.IgnoreArguments());
        }

        private IDeclaration MockDeclaration()
        {
            return MockRepository.GenerateMock<IDeclaration>();
        }
        
        [Test]
        public void ListWithTwoItems_InitAndCompareAndReturnInputList()
        {
            var declarationA = MockDeclaration();
            var declarationB = MockDeclaration();
            var collection = GetDeclarationOrderElements(declarationA, declarationB);

            var comparer = MockDeclarationComparer();
            ExpectInitWithDeclarationOrderElement(comparer, collection);
            ExpectResortNeeded(comparer,false);
            StubCompare(comparer, declarationA, declarationB, 0);
            StubCompare(comparer, declarationB, declarationA, 0);

            var result = Sort(comparer, collection);
            Assert.IsTrue(ReferenceEquals(declarationA, result[0].Declaration));
            Assert.IsTrue(ReferenceEquals(declarationB, result[1].Declaration));

            Assert.AreEqual(0, result[0].SortedIndex);
            Assert.AreEqual(0, result[1].SortedIndex);

            comparer.VerifyAllExpectations();
        }

        private void ExpectInitWithDeclarationOrderElement(IDeclarationComparer comparer,
                                                           IList<DeclarationOrderElement> orderElements)
        {
            comparer.Expect(
                c =>
                c.InitWithDeclarationOrderElement(
                    Arg<IList<DeclarationOrderElement>>
                    .Matches(l => l.Select(o => o.Declaration)
                            .SequenceEqual(orderElements.Select(e => e.Declaration)))))
                    .Repeat.Once();
        }

        private void ExpectResortNeeded(IDeclarationComparer comparer,bool needResort)
        {
            comparer.Expect(c => c.NeedResort()).Return(needResort).Repeat.Once();
        }

        [Test]
        public void ListWithTwoItems_InitAndCompareAndReturnListWithSecondItemBeforeFirst()
        {
            var declarationA = MockDeclaration();
            var declarationB = MockDeclaration();
            var collection = GetDeclarationOrderElements(declarationA,declarationB);

            var comparer = MockDeclarationComparer();
            ExpectInitWithDeclarationOrderElement(comparer, collection);
            ExpectResortNeeded(comparer, false);
            StubCompare(comparer, declarationA, declarationB, 1);
            StubCompare(comparer, declarationB, declarationA, -1);

            var result = Sort(comparer, collection);
            Assert.IsTrue(ReferenceEquals(declarationB, result[0].Declaration));
            Assert.IsTrue(ReferenceEquals(declarationA, result[1].Declaration));

            Assert.AreEqual(0, result[0].SortedIndex);
            Assert.AreEqual(1, result[1].SortedIndex);

            comparer.VerifyAllExpectations();
        }

        private static void StubCompare(IDeclarationComparer declarationComparer, IDeclaration declarationA,
                                 IDeclaration declarationB, int compareResult)
        {
            declarationComparer.Stub(c => c.Compare(Arg<DeclarationOrderElement>
                                                        .Matches(m => Equals(m.Declaration, declarationA)),
                                                    Arg<DeclarationOrderElement>
                                                        .Matches(m => Equals(m.Declaration, declarationB))))
                                                    .Return(compareResult);
        }

        [Test]
        public void ListWithThreeItems_InitAndCompareAndReturnListWithSortedItems()
        {
            var declarationA = MockDeclaration();
            var declarationB = MockDeclaration();
            var declarationC = MockDeclaration();
            var collection = GetDeclarationOrderElements(declarationA, declarationB, declarationC);

            var comparer = MockDeclarationComparer();
            ExpectInitWithDeclarationOrderElement(comparer, collection);
            ExpectResortNeeded(comparer,false);

            StubCompare(comparer, declarationA, declarationB, 1);
            StubCompare(comparer, declarationB, declarationA, -1);
            StubCompare(comparer, declarationA, declarationC, 0);
            StubCompare(comparer, declarationC, declarationA, 0);
            StubCompare(comparer, declarationC, declarationB, 1);
            StubCompare(comparer, declarationB, declarationC, -1);

            var result = Sort(comparer, collection);

            Assert.IsTrue(ReferenceEquals(declarationB, result[0].Declaration));
            Assert.IsTrue(ReferenceEquals(declarationA, result[1].Declaration));
            Assert.IsTrue(ReferenceEquals(declarationC, result[2].Declaration));

            Assert.AreEqual(0, result[0].SortedIndex);
            Assert.AreEqual(1, result[1].SortedIndex);
            Assert.AreEqual(1, result[1].SortedIndex);

            comparer.VerifyAllExpectations();
        }

        

    }
}
