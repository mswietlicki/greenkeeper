﻿using Greenkeeper.DeclarationSorting;
using JetBrains.ReSharper.Psi.Tree;
using NUnit.Framework;
using Rhino.Mocks;

namespace Greenkeeper.Tests.DeclarationSorting
{
    [TestFixture]
    public class AlphanumericDeclarationComparerTests
    {
        [TestCase("A", "A", 0)]
        [TestCase("A", "B", -1)]
        [TestCase("B", "A", 1)]
        public void CompareTest(string x, string y, int result)
        {
            Assert.AreEqual(result,Compare(x,y));
        }

        private int Compare(string x, string y)
        {
            var mockedX = MockDeclarationWithDeclaredName(x);
            var mockedY = MockDeclarationWithDeclaredName(y);

            var alphaComparer = new AlphanumericDeclarationComparer();
            return alphaComparer.Compare(mockedX, mockedY);
        }

        private DeclarationOrderElement MockDeclarationWithDeclaredName(string name)
        {
            var mocked = MockRepository.GenerateMock<IDeclaration>();
            mocked.Stub(d => d.DeclaredName).Return(name).Repeat.Any();
            return new DeclarationOrderElement(mocked);
        }
    }
}
