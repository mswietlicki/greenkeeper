﻿using Greenkeeper.DeclarationSorting;
using JetBrains.ReSharper.Psi.Tree;
using NUnit.Framework;
using Rhino.Mocks;

namespace Greenkeeper.Tests.DeclarationSorting
{
    [TestFixture]
    public class ConditionalDeclarationComparerTests
    {
        [TestCase(true, true, -1, -1)]
        [TestCase(true, true, 0, 0)]
        [TestCase(true, true, 1, 1)]
        [TestCase(false, false, 1, 0)]
        [TestCase(true, false, 1, -1)]
        [TestCase(false, true, -1, 1)]
        public void CheckCondition_IfTrueForwardToComparerIfFalseReturnEqual(bool xElementCheck, bool yElementCheck, int comparerMockCompareResult, int expectedCompareResult)
        {
            var xElement = new DeclarationOrderElement(MockRepository.GenerateMock<IDeclaration>());
            var yElement = new DeclarationOrderElement(MockRepository.GenerateMock<IDeclaration>());

            var comparerMock = MockRepository.GenerateMock<IDeclarationComparer>();
            comparerMock.Stub(c => c.Compare(xElement, yElement)).Return(comparerMockCompareResult);

            var conditionMock = MockRepository.GenerateMock<IDeclarationCondition>();
            conditionMock.Stub(c => c.CheckCondition(xElement)).Return(xElementCheck);
            conditionMock.Stub(c => c.CheckCondition(yElement)).Return(yElementCheck);

            var conditionalComparer = new ConditionalDeclarationComparer(comparerMock, conditionMock);
            var compareResult = conditionalComparer.Compare(xElement, yElement);
            Assert.AreEqual(expectedCompareResult, compareResult);
           
        }
    }
}