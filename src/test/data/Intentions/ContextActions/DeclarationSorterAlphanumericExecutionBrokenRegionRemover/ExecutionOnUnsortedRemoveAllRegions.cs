class {caret} A
{
  public string C {get; set;}
   
  public void M()
  {
  }

  public const string A;

  public const string E;

  public string E {get; set;}
  
  public const string D;
	
  #region A
  public const string B;
  
  public void L()
  {
  }

  public void A()
  {
  }

  public void C()
  {
  }
  #endregion
  
  public string D {get; set;}

  public void B()
  {
  }

  public void T()
  {
  }
  
  #region B
  public void F()
  {
  }

  public string A {get; set;}
  
  public void D()
  {
  } 
  #endregion
   
  public void O()
  {
  }
  
  public string B {get; set;}
  
  public void H()
  {
  }

  public void Q()
  {
  }
  
  public const string C;
	
  public void G()
  {
  }
}