using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

public class {caret} XmlSerializable<T> : IXmlSerializable where T : class
{    
	public void WriteXml(XmlWriter writer)
    {
        if (writer == null) throw new ArgumentNullException("writer");
        throw new NotImplementedException();
    }
	
    public T GetSerialized()
    {
        if (Serialized == null) throw new ArgumentNullException("Serialized");
        return Serialized;
    }

	private const XmlSchema _UNKNOWN_XML_SCHEMA = null;
    	
    public void ReadXml(XmlReader reader)
    {
        if (reader == null) throw new ArgumentNullException("reader");
        throw new NotImplementedException();
    }
	
    public XmlSchema GetSchema()
    {
        return _UNKNOWN_XML_SCHEMA;
    }
   
	public T Serialized { get; set; }
}
