using System;

namespace Test
{	
	interface IA
	{
	  void A();  
	  void B();
	  void C();
	  void D();
	  void E();
	}

	class {caret} A : IDisposable, IA
	{
	  public void Dispose()
      {          
      }
	  
	  public void C()
	  {  
	  }

	  public void E()
	  {  
	  }

	  public void D()
	  {  
	  }
	  
	  public void A()
	  {  
	  }
	  
	  public void B()
	  {  
	  }  
	  
	}
}