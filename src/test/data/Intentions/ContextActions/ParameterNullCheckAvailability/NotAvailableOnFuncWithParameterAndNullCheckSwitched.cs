class A
{
  public A(string arg1{off})
  {
	if (null == arg1) throw new ArgumentNullException("arg1");
  }  
  
  public A(string arg1,string arg2{off})
  {
	if (null == arg1) throw new ArgumentNullException("arg1");
	if (null == arg2) throw new ArgumentNullException("arg2");
  } 
  
  public A(string arg1,string arg2,string arg3{off})
  {
    if (null == arg2) throw new ArgumentNullException("arg2");
	if (null == arg1) throw new ArgumentNullException("arg1");
	if (null == arg3) throw new ArgumentNullException("arg3");
  } 

  public void A(string arg1{off})
  {
	if (null == arg1) throw new ArgumentNullException("arg1");
  }  
  
  public void A(string arg1,string arg2{off})
  {
	if (null == arg1) throw new ArgumentNullException("arg1");
	if (null == arg2) throw new ArgumentNullException("arg2");
  } 
  
  public void A(string arg1,string arg2,string arg3{off})
  {
    if (null == arg2) throw new ArgumentNullException("arg2");
	if (null == arg1) throw new ArgumentNullException("arg1");
	if (null == arg3) throw new ArgumentNullException("arg3");
  } 
}