class A
{
  public A(string arg1{on})
  {
  }
  
  public A(string arg1, string arg2{on})
  {
  }
  
  public A(string arg1, string arg2, string arg3{on})
  {
  }

  public void A(string arg1{on})
  {
  }
  
  public void A(string arg1, string arg2{on})
  {
  }
  
  public void A(string arg1, string arg2, string arg3{on})
  {
  }
}