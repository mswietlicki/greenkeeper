class A
{
  public A(string arg1,string arg2, int number{off})
	: this(arg2,arg1)
  {    
  }
  
  public A(string a1, string a2)
	: this(a1,"",a2)
  { 
	if (a1 == null) throw new ArgumentNullException("a1");  
  } 
  
  public A(string a1, string a2, string a3)
	: this("","","",a3)
  {    	
  } 
  
  public A(string a1, string a2, string a3, string a4)
  {    	
	if (a4 == null) throw new ArgumentNullException("a4");
  }
}