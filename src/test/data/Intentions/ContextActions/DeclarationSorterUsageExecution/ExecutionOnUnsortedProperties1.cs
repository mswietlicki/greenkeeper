class {caret} A
{  
  private void C()
  {	
  }
  
  private void E()
  {
	
  }

  private void A()
  {  
  }
  
  private string F 
  {
	get{
			C();
			D();
			E();
			return "A";
		}
  
  }
  
  private void B()
  {  
  }
  
  private string G 
  {
	get{			
			A();
			B();
			var x = F;
			return "A";
		}  
  }
  
  private void D()
  {  
  }
}