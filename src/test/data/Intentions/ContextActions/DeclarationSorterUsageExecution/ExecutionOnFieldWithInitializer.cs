class {caret} A
{
	private const string _Test1 = "Test1";
	private const string _Test2 = "Test2";

	private static readonly string[] _Tests =
	{
		new object[] { _Test2, _Test1 }
	};
}