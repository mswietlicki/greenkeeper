class {caret} A
{
  private void A()
  {
	var test = _Test;
  }

  private string Foo {get; set;}

  private void C()
  {
	A();
	D();
  }

  private void E()
  {
	A();
	var bar = Foo;
  }

  private void D()
  {
  
  }

  private string _Test;

}