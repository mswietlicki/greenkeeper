namespace Test
{	
	interface IA
	{
	  void A();  
	  void B();
	  void C();
	  void D();
	  void E();
	}

	class {caret} A : IA
	{
	  public void C()
	  {  
	  }

	  public void E()
	  {  
	  }

	  public void D()
	  {  
	  }
	  
	  public void A()
	  {  
	  }
	  
	  public void B()
	  {  
	  }  
	}
}