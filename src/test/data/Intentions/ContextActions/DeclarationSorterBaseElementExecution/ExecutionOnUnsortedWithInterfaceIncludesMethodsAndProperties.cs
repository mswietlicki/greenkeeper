namespace Test
{	
	interface IA
	{
	  void A();  
	  void B();
	  string C {get; set;}
	  string D {get; set;}
	  void E();
	}

	class {caret} A : IA
	{
	  public string C{get; set;}
	 
	  public void E()
	  {  
	  }

	  public string D {get; set;}
	  	  
	  public void A()
	  {  
	  }
	  
	  public void B()
	  {  
	  }  
	}
}